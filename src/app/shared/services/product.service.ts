import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators';
import { IProduct } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
      private http: HttpClient
  ) { }

  getProducts(): Observable<IProduct[]> {
    return this.http.get('http://54.39.188.42', {
      responseType: 'text'
    }).pipe(
      map((response: string) => {
        return JSON.parse(atob(response)) as IProduct[];
      })
    )
  }
}
