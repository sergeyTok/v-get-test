import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isEven'
})
export class IsEvenPipe implements PipeTransform {
  transform(value: any, ...args: any[]): boolean {
    if (!value) return null;
    return value % 2 === 0;
  }
}
