import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fixPrice'
})
export class FixPricePipe implements PipeTransform {
  transform(value: any, fixTo?: number): any {
    return Number(value).toFixed(fixTo || 0);
  }
}
