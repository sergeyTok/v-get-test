import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'frequentSymbolString'
})
export class FrequentSymbolStringPipe implements PipeTransform {
  transform(value: string, ...args: any[]): string {
    if (!value) return 'None';

    if (value.length === 1) return `Frequent symbol ${ value }. Count: 1`;

    let entriesObj: any[] = (Object as any)
        .entries(
          value
            .toLowerCase()
            .trim()
            .split('')
            .reduce((acc: object, current: string): object => {
              const currentAccVal: number = acc[current];

              if (!currentAccVal) {
                acc[current] = 1;
              } else {
                acc[current] = currentAccVal + 1;
              }

              return acc;
            }, {})
        );

    const maxCountSymbol: number = Math.max(...entriesObj.map((item: [string, number]) => item[1]));
    entriesObj = entriesObj.filter((item: [string, number]) => item[1] === maxCountSymbol);

    if (entriesObj.length > 1) {
      const nameMaxSymbols: string[] = entriesObj.map((item: [string, number]) => item[0]);

      return `Frequent symbols: ${nameMaxSymbols.join(', ')}. Count: ${maxCountSymbol}`;
    }

    return `Frequent symbol: ${entriesObj[0][0]}. Count: ${maxCountSymbol}`;
  }
}
