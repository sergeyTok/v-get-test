export interface IProduct {
  currency: string;
  id: number;
  image: string;
  price: number;
  sku: string;
  title: string;
  options: {
    gemstone_color: string;
    metal_color: string;
    metal_type: string;
    stone_shape: string;
  }[]
}