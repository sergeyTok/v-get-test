import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { of, Subscription } from 'rxjs/index';
import { delay } from 'rxjs/internal/operators';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[OnlyDigit]'
})
export class OnlyDigitDirective {
  private subscription: Subscription = new Subscription();

  constructor(
      private el: ElementRef,
      private control: NgControl
  ) {}

  @Input() OnlyNumber: boolean = true;

  @HostListener('keydown', ['$event']) private onKeyDown(e: KeyboardEvent) {
    if (this.OnlyNumber) {
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
          (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
          (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
          (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
          (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
          (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
      }

      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
  }

  @HostListener('paste', ['$event']) private onPaste(e: ClipboardEvent) {
    this.subscription.add(
        of(null).pipe(
            delay(0 )
        ).subscribe((): void => this.callbackChangeOrPasteInput(e))
    )
  }

  @HostListener('change', ['$event']) private onChange(e: Event) {
    this.callbackChangeOrPasteInput(e);
  }

  private static strHasLiterals(value: any): boolean {
    return value.match(/\D/gi);
  }

  private callbackChangeOrPasteInput(e: any): void {
    if (OnlyDigitDirective.strHasLiterals((e.target as any).value)) {
      e.preventDefault();
      this.clearNativeElValue();
      this.clearControl();
    }
  }

  private clearNativeElValue(): void {
    this.el.nativeElement.value = '';
  }

  private clearControl(): void {
    this.control.control.patchValue(null);
  }
}
