import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrequentSymbolStringPipe } from './pipes/frequent-symbol-string.pipe';
import { OnlyDigitDirective } from './directives/only-digit.directive';
import { IsEvenPipe } from './pipes/is-even.pipe';
import { FloorPricePipe } from './pipes/floor-price.pipe';
import { FixPricePipe } from './pipes/fix-price.pipe';

const SHARED_COMPONENTS = [
  FrequentSymbolStringPipe,
  OnlyDigitDirective,
  IsEvenPipe,
  FixPricePipe
];

@NgModule({
  declarations: [...SHARED_COMPONENTS],
  imports: [
    CommonModule
  ],
  exports: [...SHARED_COMPONENTS]
})
export class SharedModule { }
