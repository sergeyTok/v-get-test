import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './pages/test/test.component';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'test'
}, {
  path: 'test',
  component: TestComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
