import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from 'rxjs/index';
import { Router } from '@angular/router';
import { WINDOW } from '../../shared/tokens/index';
import { ProductService } from '../../shared/services/product.service';
import { IProduct } from '../../shared/interfaces/product';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  private mainForm: FormGroup;
  private task2LocalStorageValue: number;
  private $products: Observable<IProduct[]>;

  constructor(
      private _fb: FormBuilder,
      private _router: Router,
      private _productService: ProductService,
      @Inject(WINDOW) private _window: Window
  ) {}

  ngOnInit(): void {
    this.mainForm = this._fb.group({
      task1: [''],
      task2: this._fb.group({
        digit: [null, [Validators.required]]
      }),
      task3: this._fb.group({
        queryParamsArray: this._fb.array([this.getFormGroupQueryParams()])
      })
    });

    this.task2LocalStorageValue = this.getLocalStorageValueByKey('num23');

    this.$products = this._productService.getProducts();
  }

  getLocalStorageValueByKey(key: string): number {
    return Number(this._window.localStorage.getItem(key));
  }

  reloadPage(): void {
    this._window.location.reload();
  }

  onSubmitTask2(): void {
    if (this.mainForm.get('task2').valid) {
      this._window.localStorage.setItem('num23', this.mainForm.get('task2').value['digit']);
    }
  }

  setUrl(): void {
    if (this.mainForm.get('task3').get('queryParamsArray').valid) {
      const params = {};

      this.mainForm.get('task3').get('queryParamsArray').value.forEach((item: {key: string; value: string}): void => {
        params[item.key] = item.value;
      });

      this._router.navigate(['/test'], {queryParams: params}).then();
    } else {
      this.mainForm.get('task3').get('queryParamsArray').markAllAsTouched();
    }
  }

  addRowQueryParam(): void {
    (<FormArray>this.mainForm.get('task3').get('queryParamsArray')).push(this.getFormGroupQueryParams());
  }

  removeQueryParams(index: number): void {
    (<FormArray>this.mainForm.get('task3').get('queryParamsArray')).removeAt(index);
  }

  private getFormGroupQueryParams(): FormGroup {
    return this._fb.group({
      key: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      value: [null, Validators.compose([Validators.required, Validators.minLength(2)])]
    });
  }

  clearFormTask2(): void {
    const formTask2: any = this.mainForm.get('task2');

    this._window.localStorage.removeItem('num23');

    Object
        .keys(formTask2.controls)
        .forEach((controlName: string): void => {
          const control: FormControl = formTask2.get(controlName);

          control.setValue(null);
          control.markAsPristine();
          control.markAsUntouched();
          control.updateValueAndValidity();
        });
  }
}
