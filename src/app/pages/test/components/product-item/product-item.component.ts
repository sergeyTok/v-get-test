import { Component, Input, OnInit } from '@angular/core';
import { IProduct } from '../../../../shared/interfaces/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() product: IProduct;

  private objectKeys = Object.keys;

  constructor() { }

  ngOnInit(): void {
    this.checkImage();
  }

  checkImage(): void {
    fetch(this.product.image).then((response) => {
      return response;
    }, (error): void => {
      this.product.image = `https://source.unsplash.com/random?collections=${this.product.id}`;
    })
  }

  transformOptName(option: string): string {
    return option
        .replace(/_/gi, ' ')
        .split(' ')
        .map((item: string): string => {
          return `${item.substr(0, 1).toUpperCase()}${item.substr(1)}`;
        })
        .join(' ');
  }
}
