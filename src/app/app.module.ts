import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material/material.module';
import { SharedModule } from './shared/shared.module';
import { WINDOW } from './shared/tokens/index';
import { TestComponent } from './pages/test/test.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductItemComponent } from './pages/test/components/product-item/product-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ProductItemComponent
  ],
  imports: [
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    MaterialModule
  ],
  providers: [{
    provide: WINDOW,
    useFactory: (): any => {
      return window;
    }
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
